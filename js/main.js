// https://62b07877e460b79df0469b40.mockapi.io/
import { monAnController } from "./controller/monAnController.js";
import soLuong, { monAnService, username } from "./service/monAnService.js";
// import soLuong from "./service/monAnService.js";
import { spinnerService } from "./service/spinnerService.js";
import { MonAn } from "./model/monAnMoDel.js";
let sv = {
  username: "bob",
  age: 10,
};
// let name = sv.username;
// let age = sv.age;
// let { name, age } = sv;

let foodList = [];

let idFoodEdited = null;

let renderTable = (list) => {
  // render danh sách món ăn ra ngoài màn hình
  let contentHTML = "";
  for (let index = 0; index < list.length; index++) {
    let monAn = list[index];
    let contenTr = `<tr>
        <td>${monAn.id}</td>
        <td>>${monAn.name}</td>
        <td>>${monAn.price}</td>
        <td>>${monAn.description}</td>
        <td><button class="btn btn-danger" onclick="xoaMonAn(${monAn.id})">Xóa</button>
            <button class="btn btn-primary" onclick="layChiTietMonAn(${monAn.id})">Sửa</button>
        </td>    
    </tr>`;
    contentHTML += contenTr;
  }
  document.getElementById("tbody_food").innerHTML = contentHTML;
};
// hàm render
let renderDanhSachService = () => {
  monAnService
    .layDanhSachMonAn()
    .then((res) => {
      //
      spinnerService.tatLoading();
      // res.data: dữ liệu gốc từ BE trả về
      foodList = res.data.map((monAn) => {
        // monAn : object trong array từ BE trả về
        let { id, name, price, description } = monAn;
        return new MonAn(monAn.id, monAn.name, monAn.price, monAn.description);
      });
      renderTable(foodList);
    })
    .catch((err) => {
      spinnerService.tatLoading();
    });
};
// chạy lần đầu khi load lại trang
renderDanhSachService();
//    function xóa món ăn
let xoaMonAn = (maMonAn) => {
  spinnerService.batLoading();
  monAnService
    .xoaMonAn(maMonAn)
    .then((res) => {
      //
      // sau khi xóa món ăn thành công
      renderDanhSachService();
      spinnerService.tatLoading();
    })
    .catch((err) => {
      spinnerService.tatLoading();
    });
};
window.xoaMonAn = xoaMonAn;

let layChiTietMonAn = (idMonAn) => {
  // gắn id của món ăn được chọn vào biến idFoodEdited
  idFoodEdited = idMonAn;
  spinnerService.batLoading();
  monAnService
    .layThongTinChiTietMonAn(idMonAn)
    .then((res) => {
      spinnerService.tatLoading();

      // lấy dữ liệu show lên giao diện (binding dữ liệu)
      monAnController.showThongTinLenForm(res.data);
    })
    .catch((err) => {
      spinnerService.tatLoading();
    });
};
window.layChiTietMonAn = layChiTietMonAn;

// thêm mới môn ăn

let themMonAn = () => {
  let monAn = monAnController.layThongTinTuForm();
  spinnerService.batLoading();
  monAnService
    .themMoiMonAn(monAn)
    .then((res) => {
      renderDanhSachService();
      spinnerService.tatLoading();
    })
    .catch((err) => {
      alert("thất bại");
      spinnerService.tatLoading();
    });
};
window.themMonAn = themMonAn;

let capNhatMonAn = () => {
  let monAn = monAnController.layThongTinTuForm();
  console.log("monAn: ", monAn);
  console.log("idFoodEdited:", idFoodEdited);
  let newMonAn = { ...monAn, id: idFoodEdited };
  spinnerService.batLoading();
  monAnService
    .capNhatMonAn(newMonAn)
    .then((res) => {
      spinnerService.tatLoading();

      console.log(res);
      monAnController.showThongTinLenForm({
        name: "",
        price: "",
        description: "",
      });
      renderDanhSachService();
    })
    .catch((err) => {
      spinnerService.tatLoading();

      console.log(err);
    });
};
window.capNhatMonAn = capNhatMonAn;

// setTimeout(() => {
//
// }, 1000);
//

// map: array method, dùng để tạo ra mảng mới từ array ban đầu , bắt buộc phải có return khi tạo array mới

let numberArr = [2, 4, 6, 8];

let newNumberArr = [];
newNumberArr = numberArr.map((item) => {
  // console.log("item :", item);
  return item * 2;
});
console.log({ numberArr, newNumberArr });

newNumberArr.forEach((item) => {
  console.log("for each item ", item);
});
