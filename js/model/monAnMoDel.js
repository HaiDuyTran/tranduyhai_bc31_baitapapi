export class MonAn {
  constructor(_id, _name, _price, _description) {
    this.id = _id;
    this.name = _name;
    this.price = _price;
    this.description = _description;
  }
}
